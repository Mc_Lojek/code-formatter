import os
import tkinter as tk
from tkinter import ttk, filedialog, messagebox
import re


def add_tabs(k, string):
    for i in range(0, k):
        string += "    "



















class OptionsView(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent

        title_label = tk.Label(parent, text="CODE FORMATTER", fg="darkblue", bg="red")
        title_label.config(font=("Courier", 40))
        title_label.pack(pady=20, padx=20, anchor="center")

        self.filename_label = tk.Label(parent, text="FOO")
        self.filename_label.config(font=("Courier", 20))
        self.filename_label.pack()

        self.filename = "Selected file:\n<NO FILE>"
        self.filepath = "hehe.txt"
        self.filesize = 0
        self.allowed_ext = ["txt", "cpp", "h", "py", "java"]
class MainView(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent

        title_label = tk.Label(parent, text="CODE FORMATTER", fg="darkblue", bg="red")
        title_label.config(font=("Courier", 40))
        title_label.pack(pady=20, padx=20, anchor="center")

        self.filename_label = tk.Label(parent, text="FOO")
        self.filename_label.config(font=("Courier", 20))
        self.filename_label.pack()

        select_btn = tk.Button(parent, text="SELECT FILE")
        select_btn.config(font=("Courier", 20))
        select_btn.bind("<Button-1>", self.select_file)
        select_btn.pack(pady=20, padx=20)

        options_btn = tk.Button(parent, text="OPTIONS")
        options_btn.config(font=("Courier", 20))
        options_btn.bind("<Button-1>", parent.options)
        options_btn.pack(padx=20, pady=20, anchor="sw", side="left")

        start_btn = tk.Button(parent, text="START")
        start_btn.bind("<Button-1>", self.start)
        start_btn.config(font=("Courier", 20))
        start_btn.pack(padx=20, pady=20, anchor="se", side="right")

        self.filename = "Selected file:\n<NO FILE>"
        self.filepath = "hehe.txt"
        self.filesize = 0
        self.allowed_ext = ["txt", "cpp", "h", "py", "java"]















    def select_file(self, event):
        print("dziala")
        self.filepath = tk.filedialog.askopenfilename()
        if self.filepath != "":
            self.filename = self.filepath.split('/')[-1]
            self.filesize = os.path.getsize(self.filepath)
        else:
            self.filename = "<NO FILE>"
        self.filename_label.config(text="Selected file:\n" + self.filename + "\nsize:" + str(self.filesize))
    def start(self, event):
        in_file = open(self.filepath)
        out_file = open("foo.txt", 'w')

        content_string = in_file.read()
        content = list(content_string)
        for i in range(0, len(content_string)):
            # print(content_string[i:i+2])
            if content_string[i:i + 2] == '//':
                k = i
                while content_string[k] != "\n":
                    k += 1
                print(content_string[k - 2:k + 2])
                content_string = content_string[:k] + '*/\n' + content_string[k:]
        content_string = content_string.replace("//", "/*")
        content = list(re.sub('\s+', ' ', content_string))
        result = list()
        # out_file.write(''.join(content))

        depth = 0
        for i in range(len(content)):
            if ''.join(content[i - 2:i]) == '*/':
                result.append('\n')
                add_tabs(depth, result)
            elif content[i] == ' ':
                if content[i - 1] != '{' and content[i - 1] != ';' and content[i - 1] != '}' and content[
                    i - 1] != '.' and \
                        content[i + 1] != '.':
                    result.append(' ')
            elif content[i] == '{':
                depth += 1
                if content[i - 2] != ';':
                    result.append('\n')
                    add_tabs(depth - 1, result)
                result.append(content[i])
                result.append('\n')
                add_tabs(depth, result)
            elif content[i] == "}":
                depth -= 1
                result.append('\n')
                add_tabs(depth, result)
                result.append(content[i])
                if i < len(content) - 2 and content[i + 2] != '}':
                    result.append('\n')
                    add_tabs(depth, result)
            elif content[i] == ";":
                result.append(content[i])
                if content[i + 2] != '}':
                    result.append('\n')
                    add_tabs(depth, result)
            elif content[i] == "#" and i != 0:
                result.append('\n')
                result.append(content[i])
            elif ''.join(content[i:i + 15]) == 'using namespace':
                result.append('\n')
                result.append('\n')
                result.append(content[i])
            else:
                result.append(content[i])
            # print(content[i-2:i] + list(content[i]))
            # print(content[i] + str(depth))
        out_file.write(''.join(result))

        messagebox.showinfo('Executed', 'File formatted successfully')











class MainApplication(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        self.mainView = MainView(parent=self)
        self.optionsView = OptionsView(parent=self)

    def options(self, event):
        print("opcje")

        self.mainView.destroy()
        self.optionsView = OptionsView(self)
        self.pack_forget()
        self.pack()


if __name__ == "__main__":
    root = tk.Tk()
    MainApplication(root).grid(row=0, column=0)
    root.mainloop()
