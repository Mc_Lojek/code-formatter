# Author           : Maciej Lojek ( s180488@student.pg.edu.pl )
# Created On       : 22.04.2020
# Last Modified By : Maciej Lojek ( s180488@student.pg.edu.pl )
# Last Modified On : 04.05.2020
# Version          : v1.3
#
# Description      : Simple app to format your code written in different programming langs
# Opis               Prosta aplikacja do sformatowania kodu napisanego w róznych jezykach programowania
#
# Licensed under GPL (see /usr/share/common-licenses/GPL for more details
# or contact # the Free Software Foundation for a copy)

import os
import tkinter as tk
from tkinter import ttk, filedialog, messagebox, font
import re
import config


def add_tabs(k, string, size):
    if size == 0:
        for i in range(0, k):
            string += "\t"
    else:
        for i in range(0, k):
            for j in range(0, size):
                string += " "


def gen_config():
    conf_file = open("config.py", "w")
    conf_string = 'sel_lang = 0\nsel_out = 0\nsel_out_file = ""\nsel_out_path = ""\nsel_ind = 0\nsel_space_size = 0'
    conf_file.write(conf_string)


not_close_tags = [
    '<meta',
    '<li',
    '<br'
]


def check_tag(content):
    global not_close_tags
    fragment = ''.join(content)
    print(fragment)
    for tag in not_close_tags:
        if tag in fragment:
            return False
    return True


class App(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.filename = "<NO FILE>"
        self.filepath = ""
        self.filesize = 0

        try:
            f = open("config.py")
        except IOError:
            print("File not accessible")
            gen_config()
        finally:
            f.close()

        self.selected_lang = tk.IntVar()
        self.selected_lang.set(config.sel_lang)
        self.selected_out = tk.IntVar()
        self.selected_out.set(config.sel_out)
        self.selected_out_file = tk.StringVar()
        self.selected_out_file.set(config.sel_out_file)
        self.selected_out_path = tk.StringVar()
        self.selected_out_path.set(config.sel_out_path)
        self.selected_ind = tk.IntVar()
        self.selected_ind.set(config.sel_ind)
        self.space_size = tk.IntVar()
        self.space_size.set(config.sel_space_size)

        self.out_path = ""

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (MainPage, OptionsPage, PageTwo):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        if self.selected_out.get() == 1:
            self.able_path()
            print("foo")

        if self.selected_ind.get() == 1:
            self.able_spin()

        self.show_frame("MainPage")

    def show_frame(self, page_name):
        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()

    def select_file(self, page_name):
        print("dziala")
        self.filepath = tk.filedialog.askopenfilename()
        if self.filepath != "":
            self.filename = self.filepath.split('/')[-1]
            self.filesize = os.path.getsize(self.filepath)
        else:
            self.filename = "<NO FILE>"
        self.frames[page_name].filename_label.config(text="Selected file:\n" + self.filename)
        self.frames[page_name].filesize_label .config(text="size:" + str(self.filesize) + 'B')

    def start(self):

        try:
            in_file = open(self.filepath)
        except IOError:
            messagebox.showwarning('Error', 'Input file not selected!')
            return

        content_string = in_file.read()

        if self.selected_ind.get() == 0:
            self.space_size.set(0)

        if self.selected_lang.get() == 0 or self.selected_lang.get() == 1:
            result = self.cpp_java(content_string=content_string)
        elif self.selected_lang.get() == 2:
            result = self.html(content_string=content_string)
        elif self.selected_lang.get() == 3:
            result = self.python(content_string=content_string)

        if self.selected_out.get() == 0:
            try:
                out_file = open(self.filepath, 'w')
            except IOError:
                messagebox.showwarning('Error', 'Cannot open file!')
                return
        else:
            try:
                print(self.out_path)
                print(self.selected_out)
                out_file = open(self.out_path, 'w+')
            except IOError:
                messagebox.showwarning('Error', 'Cannot open file!')
                return

        out_file.write(''.join(result))
        in_file.close()
        out_file.close()

        messagebox.showinfo('Executed', 'File formatted successfully')

    def cpp_java(self, content_string):

        content = list(content_string)
        for i in range(0, len(content_string)):
            # print(content_string[i:i+2])
            if content_string[i:i + 2] == '//':
                k = i
                while content_string[k] != "\n":
                    k += 1
                content_string = content_string[:k] + '*/\n' + content_string[k:]
        content_string = content_string.replace("//", "/*")
        content = list(re.sub('\s+', ' ', content_string))
        result = list()

        depth = 0
        for i in range(len(content)):
            if ''.join(content[i - 2:i]) == '*/':
                result.append('\n')
                add_tabs(depth, result, self.space_size.get())
            elif content[i] == ' ':
                if content[i - 1] != '{' and content[i - 1] != ';' and content[i - 1] != '}' \
                        and content[i - 1] != '.' and i+1 < len(content) and content[i + 1] != '.':
                    result.append(' ')
            elif content[i] == '{':
                depth += 1
                if content[i - 2] != ';':
                    if self.selected_lang.get() == 0:
                        result.append('\n')
                    add_tabs(depth - 1, result, self.space_size.get())
                result.append(content[i])
                result.append('\n')
                add_tabs(depth, result, self.space_size.get())
            elif content[i] == "}":
                depth -= 1
                result.append('\n')
                add_tabs(depth, result, self.space_size.get())
                result.append(content[i])
                if i < len(content) - 2 and content[i + 2] != '}':
                    result.append('\n')
                    add_tabs(depth, result, self.space_size.get())
            elif content[i] == ";":
                result.append(content[i])
                if content[i + 2] != '}':
                    result.append('\n')
                    add_tabs(depth, result, self.space_size.get())
            elif content[i] == "#" and i != 0:
                result.append('\n')
                result.append(content[i])
            elif ''.join(content[i:i + 15]) == 'using namespace':
                result.append('\n')
                result.append('\n')
                result.append(content[i])
            else:
                result.append(content[i])

        return result

    def html(self, content_string):
        content = list(re.sub('>\s+<', '><', content_string))
        result = list()
        depth = 0
        for i in range(len(content)):
            if content[i] == '>' and i < len(content)-1:
                if content[i+1] == '<':
                    result.append(content[i])
                    result.append("\n")
                    if content[i+2] == '/':
                        add_tabs(depth-1, result, self.space_size.get())
                    else:
                        add_tabs(depth, result, self.space_size.get())
                else:
                    result.append(content[i])
            elif content[i] == '<':
                if content[i+1] == '/':
                    depth -= 1
                    result.append(content[i])
                else:
                    result.append(content[i])
                    check_tag(content[i:i+10])
                    if check_tag(content[i:i+10]):
                        depth += 1

            else:
                result.append(content[i])
        return result

    def python(self, content_string):
        content_string = re.sub('\n+class ', '\n\n\nclass ', content_string)
        content_string = re.sub('\n+def ', '\n\ndef ', content_string)
        content = list(re.sub('\n+    def ', '\n\n\tdef ', content_string))
        result = list()
        depth = 0
        return content


    def set_path(self):
        self.out_path = tk.filedialog.askdirectory()
        self.frames["OptionsPage"].path_entry.delete(0, tk.END)
        self.frames["OptionsPage"].path_entry.insert(0, self.out_path)

    def disable_path(self):
        self.frames["OptionsPage"].out_entry.config(state="disabled")
        self.frames["OptionsPage"].path_entry.config(state="disabled")
        self.frames["OptionsPage"].select_path_btn.config(state="disabled")
        print("ok")

    def able_path(self):
        self.frames["OptionsPage"].out_entry.config(state="normal")
        self.frames["OptionsPage"].path_entry.config(state="normal")
        self.frames["OptionsPage"].select_path_btn.config(state="normal")
        print("nie ok")

    def able_spin(self):
        self.frames["OptionsPage"].space_spin.config(state=tk.NORMAL)
        self.selected_ind.set(1)

    def disable_spin(self):
        self.frames["OptionsPage"].space_spin.config(state=tk.DISABLED)
        self.selected_ind.set(0)

    def reset_options(self):
        print("reset")
        self.selected_lang.set(0)
        self.selected_out.set(0)
        self.selected_ind.set(0)
        self.out_path = ""
        #self.frames["OptionsPage"].cpp_btn.select()
        self.frames["OptionsPage"].java_btn.deselect()
        self.frames["OptionsPage"].html_btn.deselect()
        self.frames["OptionsPage"].python_btn.deselect()
        self.frames["OptionsPage"].override_radio.select()
        self.frames["OptionsPage"].save_as_radio.deselect()
        self.frames["OptionsPage"].path_entry.delete(0, tk.END)
        self.frames["OptionsPage"].out_entry.delete(0, tk.END)
        self.able_path()
        self.selected_out_file.set("")
        self.selected_out_path.set("")
        self.disable_path()
        self.space_size.set(0)
        self.disable_spin()

    def save_options(self):
        if self.selected_out.get() == 1:
            self.out_path = self.frames["OptionsPage"].path_entry.get() + '/' + self.frames["OptionsPage"].out_entry.get()
        else:
            self.out_path = ""
        self.show_frame("MainPage")

        conf_file = open("config.py", "w")
        conf_string = "sel_lang = " + str(self.selected_lang.get()) + "\n" \
                      "sel_out = " + str(self.selected_out.get()) + "\n" \
                      "sel_out_file = \"" + self.frames["OptionsPage"].out_entry.get() + "\"\n" \
                      "sel_out_path = \"" + self.frames["OptionsPage"].path_entry.get() + "\"\n" \
                      "sel_ind = " + str(self.selected_ind.get()) + "\n" \
                      "sel_space_size = " + str(self.space_size.get())
        conf_file.write(conf_string)


class MainPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        title_label = tk.Label(self, text="CODE FORMATTER", fg="darkblue", bg="red")
        title_label.config(font=("Courier", 40))
        title_label.pack(pady=20, padx=20, anchor="center")

        self.filename_label = tk.Label(self, text="Selected file:\n" + controller.filename)
        self.filename_label.config(font=("Courier", 20))
        self.filename_label.pack()

        self.filesize_label = tk.Label(self, text='size: 0B')
        self.filesize_label.config(font=("Courier", 12))
        self.filesize_label.pack()

        select_btn = tk.Button(self, text="SELECT FILE", command=lambda: controller.select_file("MainPage"))
        select_btn.config(font=("Courier", 20))
        select_btn.pack(pady=20, padx=20)

        options_btn = tk.Button(self, text="OPTIONS", command=lambda: controller.show_frame("OptionsPage"))
        options_btn.config(font=("Courier", 20))
        options_btn.pack(padx=20, pady=20, anchor="sw", side="left")

        start_btn = tk.Button(self, text="START", command=lambda: controller.start())
        start_btn.config(font=("Courier", 20))
        start_btn.pack(padx=20, pady=20, anchor="se", side="right")


class OptionsPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        title_label = tk.Label(self, text="Options", font=("Courier", 40))
        title_label.pack(side="top", fill="x", pady=10)

        langs_frame = tk.Frame(self)
        langs_frame.pack()

        langs_label = tk.Label(langs_frame, text="Language:", font=("Courier", 14), padx=20)
        langs_label.pack(anchor=tk.W, side=tk.LEFT, expand=tk.TRUE)

        self.cpp_btn = tk.Radiobutton(langs_frame, text="C/C++", variable=controller.selected_lang, value=0,
                                       indicatoron=0, width=10)
        self.cpp_btn.pack(anchor=tk.W, side=tk.LEFT)
        self.java_btn = tk.Radiobutton(langs_frame, text="Java", variable=controller.selected_lang, value=1,
                                       indicatoron=0, width=10)
        self.java_btn.pack(anchor=tk.W, side=tk.LEFT)
        self.html_btn = tk.Radiobutton(langs_frame, text="HTML", variable=controller.selected_lang, value=2,
                                       indicatoron=0, width=10)
        self.html_btn.pack(anchor=tk.W, side=tk.LEFT)
        self.python_btn = tk.Radiobutton(langs_frame, text="Python", variable=controller.selected_lang, value=3,
                                       indicatoron=0, width=10)
        self.python_btn.pack(anchor=tk.W, side=tk.LEFT)

        self.out_file_frame = tk.Frame(self)
        self.out_file_frame.pack()

        out_label = tk.Label(self.out_file_frame, text="Output file:", font=("courier", 14), padx=20)
        out_label.pack(anchor=tk.W, side=tk.LEFT)

        self.override_radio = tk.Radiobutton(self.out_file_frame, text="Override", variable=controller.selected_out,
                                        value=0, command=lambda: controller.disable_path())
        self.override_radio.pack(anchor=tk.W, side=tk.LEFT)
        self.save_as_radio = tk.Radiobutton(self.out_file_frame, text="Save as:", variable=controller.selected_out,
                                       value=1, command=lambda: controller.able_path())
        self.save_as_radio.pack(anchor=tk.W, side=tk.LEFT)

        self.out_entry = tk.Entry(self.out_file_frame, width=25, state=tk.DISABLED, textvariable=controller.selected_out_file)
        self.out_entry.pack(anchor=tk.W, side=tk.LEFT)

        # output file ui
        path_frame = tk.Frame(self)
        path_frame.pack()
        path_label = tk.Label(path_frame, text="Output directory:", font=("Courier", 14), padx=20)
        path_label.pack(anchor=tk.W, side=tk.LEFT)
        self.path_entry = tk.Entry(path_frame, width=50, state=tk.DISABLED, textvariable=controller.selected_out_path)
        self.path_entry.pack(anchor=tk.W, side=tk.LEFT)
        self.select_path_btn = tk.Button(path_frame, text="select",
                                         command=lambda: controller.set_path(), state=tk.DISABLED)
        self.select_path_btn.pack(anchor=tk.W, padx=20, side=tk.LEFT)

        #indentation select
        ind_frame = tk.Frame(self)
        ind_frame.pack()
        ind_label = tk.Label(ind_frame, text="Indentation sign:", font=("Courier", 14), padx=20)
        ind_label.pack(side=tk.LEFT)
        self.tab_radio = tk.Radiobutton(ind_frame, text="Tab", variable=controller.selected_ind, value=0,
                                        command=lambda: controller.disable_spin())
        self.tab_radio.pack(side=tk.LEFT)
        self.space_radio = tk.Radiobutton(ind_frame, text="Space", variable=controller.selected_ind, value=1,
                                          command=lambda: controller.able_spin())
        self.space_radio.pack(side=tk.LEFT)
        self.space_spin = tk.Spinbox(ind_frame, from_=1, to=10, textvariable=controller.space_size,
                                     width=10, state=tk.DISABLED)
        self.space_spin.pack(side=tk.LEFT, padx=20)

        buttons_frame = tk.Frame(self)
        buttons_frame.pack(anchor=tk.S)

        # button back
        reset_btn = tk.Button(self, text="RESET", command=lambda: controller.reset_options())
        reset_btn.config(font=("Courier", 20))
        reset_btn.pack(padx=20, pady=20, side=tk.LEFT, anchor="s")

        # button back
        save_btn = tk.Button(self, text="SAVE", command=lambda: controller.save_options())
        save_btn.config(font=("Courier", 20))
        save_btn.pack(padx=20, pady=20, side=tk.RIGHT, anchor="se")


class PageTwo(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="This is page 2", font=("Courier", 40))
        label.pack(side="top", fill="x", pady=10)
        button = tk.Button(self, text="Go to the start page",
                           command=lambda: controller.show_frame("StartPage"))
        button.pack()


if __name__ == "__main__":
    app = App()
    app.title("Code formatter")
    app.geometry("640x480")
    app.mainloop()
